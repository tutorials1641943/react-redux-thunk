import {createSlice, configureStore, getDefaultMiddleware} from '@reduxjs/toolkit';

const counterSlice = createSlice({
    name: 'counter',
    initialState: {
        names: [],
        size: 0,
        name: "asdfads"
    }, 
    reducers: {
        addName: (state) => {
            const newName = state.name 
            state.names.push(newName)
            state.size = state.size + 1 
            state.name = ""
        },
        setName: (state, action) => {
            console.log(action)
            state.name = action.payload
        },
        removeName: (state, action) => {
            state.names.splice(action.payload, 1)
            state.size = state.size - 1 
        }, 
        updateNames: (state, action) => {
            //fetch name from the api server 
            const fetchedNames = action.payload 
            console.log("dispatching updateNames")
            console.log(fetchedNames)

            state.names = state.names.concat(fetchedNames)
        }
    }
})

export const {addName, setName, removeName, updateNames} = counterSlice.actions

export const store = configureStore({
    reducer: counterSlice.reducer,
})