import './App.css'
import { addName, removeName, setName, updateNames, store } from './store'
import { useDispatch, useSelector } from 'react-redux'
const delay = (time) => {
  return new Promise(resolve => setTimeout(resolve, time));
}

const fetchDatas = (dummy) => async dispatch => {
  await delay(1000)
  const data = ["Jane", "Marry", "John", "Black Smith"]

  dispatch(updateNames(data))
}

const CustomComponent = () => {
  const names = useSelector((state) => state.names)
  const name = useSelector(state => state.name)

  const dispatch = useDispatch()

  const inputHandler = (e) => {
    dispatch(setName(e.target.value))
  }

  const addNameHandler = () => {
    dispatch(addName())
  }

  const getDatas = () => {
    dispatch(fetchDatas("dummy data!!!"))
  }

  return (
    <div>
      {names.map((name) => {
        return <div>{name}</div>
      })}
      <input type="text" onChange={inputHandler} value={name} />
      <div>
      <button onClick={addNameHandler}>Add name</button>
      <button onClick={getDatas}> GET DATAS FROM SERVER!!!</button>
      </div>
    </div>
  )
}

function App() {
  return (
    <div>
      Hello World!
      <CustomComponent />
    </div>
  )
}

export default App
